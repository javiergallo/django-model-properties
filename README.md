Django Model Properties
=======================

Model properties are read-only field-like derived attributes for models.

Model properties help Django developers reduce constraints or functional dependencies in models, reduce database storage, avoid duplication of code or logic, and make use of derived attributes at both database level (in queries) and Python level (with model instances).

Basic Usage
-----------
A model property is attached to a model to link a database expression to a semantically equivalent Python function. The function can be provided by the caller, but if it's not, the property will try to compile a function by itself.

Example:

```python
from model_properties import ModelProperty
from model_properties.managers import AnnotableModelManager

from django.db.models import CharField, F, Model, Value
from django.db.models.functions import Concat

class Person(Model):
    first_name = CharField(max_length=128)
    last_name = CharField(max_length=128)

    full_name = ModelProperty(
        Concat(F('first_name'), Value(' '), F('last_name'))

        # For this case, `func` is optional since the expression above can be
        # easily compiled by the property itself:
        # func=lambda person: person.first_name + ' ' + person.last_name
    )

    objects = AnnotableModelManager()

Person.objects.create(first_name='Alice', last_name='Liddell')

alice = Person.objects.filter(full_name__iexact='alice liddell').first()
print(alice.full_name)  # 'Alice Liddell'
```

Already existing properties in models may also be replaced by model properties to link their logic to database expressions.

Example:

```python
from model_properties import model_property
from model_properties.managers import AnnotableModelManager

from django.db.models import CharField, F, Model, Value
from django.db.models.functions import Concat

class Person(Model):
    first_name = CharField(max_length=128)
    last_name = CharField(max_length=128)

    objects = AnnotableModelManager()

    @model_property(Concat(F('first_name'), Value(' '), F('last_name')))
    def full_name(self):
        return self.first_name + ' ' + self.last_name

Person.objects.create(first_name='Alice', last_name='Liddell')

alice = Person.objects.filter(full_name__iexact='alice liddell').first()
print(alice.full_name)  # 'Alice Liddell'
```
