import os
from setuptools import setup


def open_file(filename):
    return open(os.path.join(os.path.dirname(__file__), filename))


requirements = [
    line.strip()
    for line in open_file('requirements.txt').readlines()
]

setup(
    name='django-model-properties',
    version='0.6.1',
    author='Javier Gallo',
    author_email='javier_rooster@yahoo.com.ar',
    description='Django Model Properties',
    url='https://bitbucket.org/winclap/django-model-properties',
    packages=['model_properties'],
    license='MIT',
    long_description=open_file('README.md').read(),
    install_requires=requirements
)
