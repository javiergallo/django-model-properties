import django
import sys

from django.conf import settings
from django.test.runner import DiscoverRunner

settings.configure(
    DEBUG=True,
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.sqlite3'
        }
    },
    INSTALLED_APPS=('model_properties.tests.app',)
)

django.setup()

test_runner = DiscoverRunner(verbosity=1)

failures = test_runner.run_tests(['model_properties'])
if failures:
    sys.exit(failures)
