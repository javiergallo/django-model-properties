import collections
import copy

from django.db import models

from . import ModelAnnotation
from .querysets import AnnotableModelQuerySet


class AnnotableModelManager(models.Manager):
    """
    A manager for _annotable models_. An annotable model is a model with
    instances of `ModelAnnotation` attached to it. Each annotation is added to
    the manager's queryset.
    """

    _queryset_class = AnnotableModelQuerySet

    _model_annotations = None

    def _unordered_model_annotations(self):
        model_annotations = {}
        for attr_name in dir(self.model):
            attr_value = getattr(self.model, attr_name)
            if isinstance(attr_value, ModelAnnotation):
                model_annotations[attr_name] = attr_value
        return model_annotations

    def _model_annotations_ordered_by_dep(self):
        model_annotations = self._unordered_model_annotations()

        # Perform topological sorting to model annotation names
        ordered_attr_names = []
        visited_attr_names = set()

        def visit(attr_name):
            model_annotation = model_annotations.get(attr_name, None)
            if model_annotation:
                for literal_name in model_annotation.literal_names:
                    if attr_name not in visited_attr_names:
                        visit(literal_name)
            ordered_attr_names.append(attr_name)
            visited_attr_names.add(attr_name)

        for attr_name in model_annotations:
            if attr_name not in visited_attr_names:
                visit(attr_name)

        # Return ordered model annotations
        return collections.OrderedDict([
            (attr_name, model_annotations[attr_name])
            for attr_name in ordered_attr_names
            if attr_name in model_annotations
        ])

    @property
    def model_annotations(self):
        if self._model_annotations is None:
            self._model_annotations = self._model_annotations_ordered_by_dep()
        return self._model_annotations

    def get_queryset(self):
        qs = super(AnnotableModelManager, self).get_queryset()
        for name, model_annotation in self.model_annotations.items():
            qs = qs.annotate(**{name: model_annotation.expr})
        return qs

    def project_model_annotations(self, *names):
        new_manager = copy.copy(self)
        new_manager._model_annotations = collections.OrderedDict([
            (name, model_annotation)
            for name, model_annotation in self.model_annotations.items()
            if name in names
        ])
        return new_manager.get_queryset()

    def clear_model_annotations(self):
        return self.project_model_annotations()
