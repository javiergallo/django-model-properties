import operator

from django.db.models import F, Value
from django.db.models.expressions import Combinable, CombinedExpression
from django.db.models.constants import LOOKUP_SEP
from django.db.models.functions import (Coalesce, Concat, ConcatPair, Length,
                                        Lower, Substr, Upper)


class ArityException(Exception):
    pass


class BaseDBExprCompiler():
    """
    Base class for database expression to python function compilers. Method
    `compile` must be overridden.
    """

    @classmethod
    def compile_source_expressions(cls, expr):
        return [DBExprCompiler().compile(source_expr)
                for source_expr in expr.get_source_expressions()]

    def compile(self, expr):
        raise NotImplementedError


class BaseFuncCompiler(BaseDBExprCompiler):
    """
    Base class for database function to python function compilers. Method
    `eval` must be overridden.
    """

    @staticmethod
    def eval(*args):
        raise NotImplementedError


class BaseUnaryFuncCompiler(BaseFuncCompiler):
    """
    Base class for unary database function (e.g. `Length`) to python function
    compilers. Method `eval` takes a single argument and must be overridden.
    """

    def compile(self, expr):
        source_func = self.compile_source_expressions(expr).pop()

        def func(instance):
            return type(self).eval(source_func(instance))

        return func


class BaseBinaryFuncCompiler(BaseFuncCompiler):
    """
    Base class for binary database function (e.g. `ConcatPair`) to python
    function compilers. Method `eval` takes two arguments and must be
    overridden.
    """

    def compile(self, expr):
        source_funcs = self.compile_source_expressions(expr)

        def func(instance):
            left, right = [
                source_func(instance) for source_func in source_funcs
            ]
            return type(self).eval(left, right)

        return func


class BaseNAryFuncCompiler(BaseFuncCompiler):
    """
    Base class for n-ary database function (e.g. `Concat`) to python function
    compilers. Method `eval` takes n arguments and must be overridden.
    """

    arities = None

    def compile(self, expr):
        source_funcs = self.compile_source_expressions(expr)
        if self.arities is not None and len(source_funcs) not in self.arities:
            raise ArityException('Valid arities are {}'.format(
                ', '.join(self.arities)
            ))

        def func(instance):
            values = [source_func(instance) for source_func in source_funcs]
            return type(self).eval(values)

        return func


class ValueCompiler(BaseDBExprCompiler):
    def compile(self, expr):
        def func(instance):
            return expr.value

        return func


class FCompiler(BaseDBExprCompiler):
    @staticmethod
    def _get(instance, attr_names):
        value = instance
        for attr_name in attr_names:
            value = getattr(value, attr_name)
        return value

    def compile(self, expr):
        def func(instance):
            return self._get(instance, expr.name.split(LOOKUP_SEP))

        return func


class ConcatCompiler(BaseNAryFuncCompiler):
    @staticmethod
    def eval(values):
        return ''.join(values)


class ConcatPairCompiler(BaseBinaryFuncCompiler):
    @staticmethod
    def eval(left, right):
        return str(left) + str(right)


class CoalesceCompiler(BaseNAryFuncCompiler):
    @staticmethod
    def eval(values):
        result = None
        for value in values:
            if value is not None:
                result = value
                break
        return result


class LengthCompiler(BaseUnaryFuncCompiler):
    @staticmethod
    def eval(value):
        return len(value)


class LowerCompiler(BaseUnaryFuncCompiler):
    @staticmethod
    def eval(value):
        return value.lower()


class SubstrCompiler(BaseNAryFuncCompiler):
    arities = {2, 3}

    @staticmethod
    def eval(values):
        value = values[0]
        pos = values[1] - 1
        if len(values) > 2:
            length = values[2]
            result = value[pos:pos + length]
        else:
            result = value[pos:]
        return result


class UpperCompiler(BaseUnaryFuncCompiler):
    @staticmethod
    def eval(value):
        return value.upper()


class CombinedExpressionCompiler(BaseFuncCompiler):
    CONNECTOR_MAPPING = {
        Combinable.ADD: operator.add,
        Combinable.SUB: operator.sub,
        Combinable.MUL: operator.mul,
        Combinable.DIV: operator.truediv,
        Combinable.POW: operator.pow,
        Combinable.MOD: operator.mod,
        Combinable.BITAND: operator.and_,
        Combinable.BITOR: operator.or_
    }

    def compile(self, expr):
        source_funcs = self.compile_source_expressions(expr)

        def func(instance):
            left, right = [
                source_func(instance) for source_func in source_funcs
            ]
            result = self.CONNECTOR_MAPPING[expr.connector](left, right)
            return result

        return func


class DBExprCompilationException(Exception):
    pass


class DBExprCompiler(BaseDBExprCompiler):
    """
    The _universal_ database expression to python function compiler.

    Usage E.g.:
    ```
    from django.db.models import CharField, F, Model, Value
    from django.db.models.functions import Concat

    class Person(Model):
        first_name = CharField(...)
        last_name = CharField(...)

    alice = Person.objects.create(first_name='Alice', last_name='Liddell')

    expr = Concat(F('first_name'), Value(' '), F('last_name'))
    get_full_name = DBExprCompiler().compile(expr)

    get_full_name(alice)  # 'Alice Liddell'
    ```
    """

    EXPR_COMPILER_MAPPING = [
        (Value, ValueCompiler),
        (F, FCompiler),
        (Coalesce, CoalesceCompiler),
        (Concat, ConcatCompiler),
        (ConcatPair, ConcatPairCompiler),
        (Length, LengthCompiler),
        (Lower, LowerCompiler),
        (Substr, SubstrCompiler),
        (Upper, UpperCompiler),
        (CombinedExpression, CombinedExpressionCompiler),
    ]

    def compile(self, expr):
        func = None
        for expr_class, compiler_class in self.EXPR_COMPILER_MAPPING:
            if isinstance(expr, expr_class):
                func = compiler_class().compile(expr)
                break

        if func is None:
            raise DBExprCompilationException(
                'Unable to compile value/object of type {}'.format(type(expr))
            )

        return func
