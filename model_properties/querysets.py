from django.db import models, transaction
from django.db.models import sql
from django.db.models.sql.constants import CURSOR


class AnnotableModelQuery(sql.Query):
    """
    A `Query` for _annotable models_.
    """
    def clear_annotations(self):
        self._annotations = None


class AnnotableModelUpdateQuery(sql.UpdateQuery, AnnotableModelQuery):
    """
    An improved `UpdateQuery` for _annotable models_.
    """

    def add_update_fields(self, values_seq):
        """
        Appends a sequence of (field, model, value) triples to the internal
        list that will be used to generate the UPDATE query. Might be more
        usefully called `add_update_targets()` to hint at the extra
        information here.
        """
        for field, model, val in values_seq:
            if hasattr(val, 'resolve_expression'):
                # Resolve expressions here so that annotations are no longer
                # needed. See `AnnotableModelQuerySet.update`.
                val = val.resolve_expression(self,
                                             allow_joins=False,
                                             for_save=True)
            self.values.append((field, model, val))


class AnnotableModelQuerySet(models.QuerySet):
    """
    A improved `QuerySet` for _annotable models_.
    """

    def __init__(self, model=None, query=None, using=None, hints=None):
        super(AnnotableModelQuerySet, self).__init__(
            model, query or AnnotableModelQuery(model), using, hints
        )

    def update(self, **kwargs):
        """
        Updates all elements in the current QuerySet, setting all the given
        fields to the appropriate values.
        """
        assert self.query.can_filter(), \
            "Cannot update a query once a slice has been taken."
        self._for_write = True

        # **NOTICE**: for new Django versions,
        # `clone(self, klass=None, **kwargs)` was split into `clone(self)` and
        # `chain(self, klass=None)`. Check this out:
        # https://github.com/django/django/commit/6155bc4a51d44afa096c4c00766cbfb9ba9d660c
        if callable(getattr(self.query, 'chain', None)):
            query = self.query.chain(AnnotableModelUpdateQuery)
        else:
            query = self.query.clone(AnnotableModelUpdateQuery)
        query.add_update_values(kwargs)

        # Clear any annotations so that they won't be present in subqueries.
        # **NOTICE**: `QuerySet.update` from Django versions before 1.11
        # crashes when trying to update a field in terms of an annotation.
        # Refs:
        # * https://code.djangoproject.com/ticket/18580
        # * https://code.djangoproject.com/ticket/19513
        # * https://github.com/django/django/commit/a84344bc539c66589c8d4fe30c6ceaecf8ba1af3
        query.clear_annotations()

        with transaction.atomic(using=self.db, savepoint=False):
            rows = query.get_compiler(self.db).execute_sql(CURSOR)
        self._result_cache = None
        return rows
    update.alters_data = True
