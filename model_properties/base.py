from django.db.models import F
from django.db.models.constants import LOOKUP_SEP
from django.db.models.expressions import BaseExpression
from django.db.models.query_utils import Q

from .compilers import DBExprCompiler


class ModelAnnotation(object):
    """
    An annotation attached to a model definition. It requires a database
    expression.

    Usage E.g.:
    ```
    from django.db.models import CharField, F, Model, Value
    from django.db.models.functions import Concat

    class Person(Model):
        first_name = CharField(...)
        last_name = CharField(...)

        full_name = ModelAnnotation(
            Concat(F('first_name'), Value(' '), F('last_name'))
        )

        objects = AnnotableModelManager()

    Person.objects.create(first_name='Alice', last_name='Liddell')
    alice = Person.objects.filter(full_name__iexact='alice liddell').first()
    ```
    """

    _value = None
    _literals = None
    _literal_names = None

    def __init__(self, expr):
        self._expr = expr

    @property
    def expr(self):
        return self._expr

    def _get_expr_literals(self, expr):
        if isinstance(expr, (F, Q)):
            literals = {expr}
        elif isinstance(expr, BaseExpression):
            literals = set()
            for source_expr in expr.get_source_expressions():
                literals.update(self._get_expr_literals(source_expr))
        else:
            raise TypeError(
                'Unrecognized expression type {}'.format(type(expr))
            )
        return literals

    @property
    def literals(self):
        if self._literals is None:
            self._literals = self._get_expr_literals(self.expr)
        return self._literals

    @property
    def literal_names(self):
        if self._literal_names is None:
            self._literal_names = set()
            for literal in self.literals:
                if isinstance(literal, F):
                    self._literal_names.add(literal.name)
                elif isinstance(literal, Q):
                    for keyword, _ in literal.children:
                        self._literal_names.add(keyword.split(LOOKUP_SEP)[0])
        return self._literal_names

    def __get__(self, instance, owner):
        if instance is None:
            value = self
        else:
            value = self._value
        return value

    def __set__(self, instance, value):
        self._value = value


class ModelProperty(ModelAnnotation):
    """
    A model annotation that links a database expression to a semantically
    equivalent Python function. The function can be provided by the caller. If
    it's not, an attempt to compile an equivalent function will take place.

    Usage E.g.:
    ```
    from django.db.models import CharField, F, Model, Value
    from django.db.models.functions import Concat

    class Person(Model):
        first_name = CharField(...)
        last_name = CharField(...)

        full_name = ModelProperty(
            Concat(F('first_name'), Value(' '), F('last_name'))

            # This is optional:
            # func=lambda person: person.first_name + ' ' + person.last_name
        )

        objects = AnnotableModelManager()

    Person.objects.create(first_name='Alice', last_name='Liddell')

    alice = Person.objects.filter(full_name__iexact='alice liddell').first()
    print(alice.full_name)  # 'Alice Liddell'
    ```
    """

    def __init__(self, expr, func=None):
        super(ModelProperty, self).__init__(expr)

        self._func = func

    @property
    def func(self):
        if self._func is None:
            self._func = DBExprCompiler().compile(self.expr)
        return self._func

    def __get__(self, instance, owner):
        if instance is None:
            value = self
        else:
            value = self.func(instance)
        return value


def model_property(expr):
    """
    Returns a `ModelProperty` constructor given a database expression. The
    constructor requires an equivalent python function.

    Usage E.g.:
    ```
    from django.db.models import CharField, F, Model, Value
    from django.db.models.functions import Concat

    class Person(Model):
        first_name = CharField(...)
        last_name = CharField(...)

        objects = AnnotableModelManager()

        @model_property(Concat(F('first_name'), Value(' '), F('last_name')))
        def full_name(self):
            return self.first_name + ' ' + self.last_name

    Person.objects.create(first_name='Alice', last_name='Liddell')

    alice = Person.objects.filter(full_name__iexact='alice liddell').first()
    print(alice.full_name)  # 'Alice Liddell'
    ```
    """
    def create_model_property(func):
        return ModelProperty(expr, func)
    return create_model_property
