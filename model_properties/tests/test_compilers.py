import inspect

from django.db.models import F, Value
from django.db.models.functions import (Coalesce, Concat, Length, Lower,
                                        Substr, Upper)

from django.test import TestCase

from ..compilers import (CoalesceCompiler, ConcatCompiler, FCompiler,
                         LengthCompiler, LowerCompiler, SubstrCompiler,
                         UpperCompiler, ValueCompiler)

from .app.models import Person


class BaseCompilerTestCase(TestCase):
    compiler_class = None

    def instantiate_compiler(self):
        return self.compiler_class()

    def setUp(self):
        self.instance = Person(first_name='Alice', last_name='Liddell')
        self.compiler = self.instantiate_compiler()

    def assertCompilationIsValid(self, expr):
        func = self.compiler.compile(expr)
        if not callable(func):
            msg = 'Compiled expression "{}" not callable'
            raise AssertionError(msg.format(expr))

        args_len = len(inspect.getargspec(func).args)
        if args_len != 1:
            msg = 'Number of arguments of compiled expression {} != 1'
            raise AssertionError(msg.format(args_len))

    def assertEvalEquals(self, expr, value):
        self.assertEquals(self.compiler.compile(expr)(self.instance), value)


class ValueCompilerTestCase(BaseCompilerTestCase):
    compiler_class = ValueCompiler

    def test_compilation(self):
        self.assertCompilationIsValid(Value('foobar'))

    def test_evaluation(self):
        # Test different value types
        for value in [True, 1, 1.5, 'foobar']:
            self.assertEvalEquals(Value(value), value)


class FCompilerTestCase(BaseCompilerTestCase):
    compiler_class = FCompiler

    def test_compilation(self):
        self.assertCompilationIsValid(F('first_name'))

    def test_evaluation(self):
        for attr_name in ['first_name', 'last_name']:
            self.assertEvalEquals(
                F(attr_name),
                getattr(self.instance, attr_name)
            )


class ConcatCompilerTestCase(BaseCompilerTestCase):
    compiler_class = ConcatCompiler

    def test_compilation(self):
        self.assertCompilationIsValid(Concat('foo', 'bar'))

    def test_free_evaluation(self):
        for values in [['foo', 'bar'], ['foo', 'bar', 'baz']]:
            exprs = tuple(Value(value) for value in values)
            self.assertEvalEquals(Concat(*exprs), ''.join(values))

    def test_bound_evaluation(self):
        self.assertEvalEquals(
            Concat(F('first_name'), Value(' '), F('last_name')),
            'Alice Liddell'
        )

# TODO ConcatPairCompiler


class CoalesceCompilerTestCase(BaseCompilerTestCase):
    compiler_class = CoalesceCompiler

    def test_compilation(self):
        self.assertCompilationIsValid(Coalesce(Value(None), Value(None)))

    def test_free_evaluation(self):
        self.assertEvalEquals(Coalesce(Value(None), Value(None)), None)
        self.assertEvalEquals(Coalesce(Value(None), Value('foo')), 'foo')
        self.assertEvalEquals(Coalesce(Value('bar'), Value('baz')), 'bar')
        self.assertEvalEquals(
            Coalesce(Value(None), Value(None), Value('baz')),
            'baz'
        )

    def test_bound_evaluation(self):
        self.assertEvalEquals(Coalesce(Value(None), F('first_name')), 'Alice')
        self.assertEvalEquals(
            Coalesce(F('last_name'), F('first_name')),
            'Liddell'
        )
        self.assertEvalEquals(
            Coalesce(Value(None), Value(None), F('first_name')),
            'Alice'
        )


class LengthCompilerTestCase(BaseCompilerTestCase):
    compiler_class = LengthCompiler

    def test_compilation(self):
        self.assertCompilationIsValid(Length(Value('foo')))

    def test_free_evaluation(self):
        self.assertEvalEquals(Length(Value('')), 0)
        self.assertEvalEquals(Length(Value('foo')), 3)

    def test_bound_evaluation(self):
        self.assertEvalEquals(Length(F('first_name')), 5)
        self.assertEvalEquals(Length(F('last_name')), 7)


class LowerCompilerTestCase(BaseCompilerTestCase):
    compiler_class = LowerCompiler

    def test_compilation(self):
        self.assertCompilationIsValid(Lower(Value('Foo')))

    def test_free_evaluation(self):
        self.assertEvalEquals(Lower(Value('')), '')
        self.assertEvalEquals(Lower(Value('Foo')), 'foo')
        self.assertEvalEquals(Lower(Value('BAR')), 'bar')
        self.assertEvalEquals(Lower(Value('bar')), 'bar')

    def test_bound_evaluation(self):
        self.assertEvalEquals(Lower(F('first_name')), 'alice')
        self.assertEvalEquals(Lower(F('last_name')), 'liddell')


class SubstrCompilerTestCase(BaseCompilerTestCase):
    compiler_class = SubstrCompiler

    def test_compilation(self):
        self.assertCompilationIsValid(Substr(Value('foobar'), Value(4)))

    def test_free_evaluation(self):
        self.assertEvalEquals(Substr(Value(''), Value(4)), '')
        self.assertEvalEquals(
            Substr(Value('foobarbaz'), Value(1)),
            'foobarbaz'
        )
        self.assertEvalEquals(Substr(Value('foobarbaz'), Value(4)), 'barbaz')
        self.assertEvalEquals(Substr(Value(''), Value(4), Value(3)), '')
        self.assertEvalEquals(
            Substr(Value('foobarbaz'), Value(4), Value(0)),
            ''
        )
        self.assertEvalEquals(
            Substr(Value('foobarbaz'), Value(4), Value(3)),
            'bar'
        )

    def test_bound_evaluation(self):
        self.assertEvalEquals(Substr(F('first_name'), Value(3)), 'ice')
        self.assertEvalEquals(
            Substr(F('first_name'), Value(1), Value(3)), 'Ali'
        )

        # TODO test position and length with fields (F expressions) instead of
        # just values

    def test_args_casting_evaluation(self):
        self.assertEvalEquals(Substr(Value('foobarbaz'), 4), 'barbaz')
        self.assertEvalEquals(Substr(Value('foobarbaz'), 4, 3), 'bar')


class UpperCompilerTestCase(BaseCompilerTestCase):
    compiler_class = UpperCompiler

    def test_compilation(self):
        self.assertCompilationIsValid(Upper(Value('Foo')))

    def test_free_evaluation(self):
        self.assertEvalEquals(Upper(Value('')), '')
        self.assertEvalEquals(Upper(Value('Foo')), 'FOO')
        self.assertEvalEquals(Upper(Value('bar')), 'BAR')
        self.assertEvalEquals(Upper(Value('BAR')), 'BAR')

    def test_bound_evaluation(self):
        self.assertEvalEquals(Upper(F('first_name')), 'ALICE')
        self.assertEvalEquals(Upper(F('last_name')), 'LIDDELL')

# TODO test cases for CombinedExpressionCompiler and DBExprCompiler
