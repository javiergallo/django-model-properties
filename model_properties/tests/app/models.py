from django.db.models import (CharField, ExpressionWrapper, F, Model,
                              TextField, Value)
from django.db.models.functions import Concat

from ...base import ModelAnnotation, ModelProperty, model_property
from ...managers import AnnotableModelManager


class Person(Model):
    FULL_NAME_EXPR = Concat(F('first_name'), Value(' '), F('last_name'))

    first_name = CharField(max_length=128)
    last_name = CharField(max_length=128)
    details = TextField(default='')

    full_name = ModelProperty(FULL_NAME_EXPR)
    full_name_as_annotation = ModelAnnotation(FULL_NAME_EXPR)

    @model_property(
        Concat(
            F('full_name'), Value(': '),
            ExpressionWrapper(F('details'), output_field=CharField())
        )
    )
    def badge(self):
        return '{}: {}'.format(self.full_name, self.details)

    objects = AnnotableModelManager()
