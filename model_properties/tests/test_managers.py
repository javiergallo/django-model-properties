from django.test import TestCase

from ..querysets import AnnotableModelQuerySet

from .app.models import Person


class AnnotableModelManagerTestCase(TestCase):
    def setUp(self):
        self.alice = Person.objects.create(
            first_name='Alice', last_name='Liddell',
            details="The woman who may have inspired Alice's Adventures in "
                    "Wonderland."
        )
        self.bob = Person.objects.create(
            first_name='Bob', last_name='Dylan',
            details="An American singer-songwriter, author, and painter."
        )
        self.people = [self.alice, self.bob]

    def test_queryset(self):
        queryset = Person.objects.get_queryset()
        self.assertIsInstance(queryset, AnnotableModelQuerySet)
        self.assertEquals(list(queryset), self.people)
        self.assertEquals(
            list(queryset.values()),
            [
                {
                    'id': person.pk,
                    'first_name': person.first_name,
                    'last_name': person.last_name,
                    'details': person.details,
                    'full_name': person.full_name,
                    'full_name_as_annotation': person.full_name,
                    'badge': person.badge
                } for person in self.people
            ]
        )

    def test_model_annotations_projection(self):
        queryset = Person.objects.project_model_annotations('full_name',
                                                            'badge')
        self.assertIsInstance(queryset, AnnotableModelQuerySet)
        self.assertEquals(list(queryset), self.people)
        self.assertEquals(
            list(queryset.values()),
            [
                {
                    'id': person.pk,
                    'first_name': person.first_name,
                    'last_name': person.last_name,
                    'details': person.details,
                    'full_name': person.full_name,
                    'badge': person.badge
                } for person in self.people
            ]
        )

        queryset = Person.objects.project_model_annotations('full_name')
        self.assertIsInstance(queryset, AnnotableModelQuerySet)
        self.assertEquals(list(queryset), self.people)
        self.assertEquals(
            list(queryset.values()),
            [
                {
                    'id': person.pk,
                    'first_name': person.first_name,
                    'last_name': person.last_name,
                    'details': person.details,
                    'full_name': person.full_name
                } for person in self.people
            ]
        )

        queryset = Person.objects.project_model_annotations()
        self.assertIsInstance(queryset, AnnotableModelQuerySet)
        self.assertEquals(list(queryset), self.people)
        self.assertEquals(
            list(queryset.values()),
            [
                {
                    'id': person.pk,
                    'first_name': person.first_name,
                    'last_name': person.last_name,
                    'details': person.details
                } for person in self.people
            ]
        )

        queryset = Person.objects.project_model_annotations(
            'full_name'
        ).filter(full_name__iexact='alice liddell')
        self.assertEquals(
            list(queryset.values()),
            [
                {
                    'id': self.alice.pk,
                    'first_name': self.alice.first_name,
                    'last_name': self.alice.last_name,
                    'details': self.alice.details,
                    'full_name': self.alice.full_name
                }
            ]
        )
        self.assertEquals(queryset.first(), self.alice)

    def test_model_annotations_clearing(self):
        queryset = Person.objects.clear_model_annotations()
        self.assertIsInstance(queryset, AnnotableModelQuerySet)
        self.assertEquals(list(queryset), self.people)
        self.assertEquals(
            list(queryset.values()),
            [
                {
                    'id': person.pk,
                    'first_name': person.first_name,
                    'last_name': person.last_name,
                    'details': person.details
                } for person in self.people
            ]
        )
