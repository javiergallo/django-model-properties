from django.db.models import F, Value
from django.db.models.functions import Concat
from django.test import TestCase

from .app.models import Person


class AnnotableModelQuerySetTestCase(TestCase):
    def setUp(self):
        self.alice = Person.objects.create(first_name='Alice',
                                           last_name='Liddell')
        self.queryset = Person.objects.get_queryset()

    def test_update(self):
        alice_qs = self.queryset.filter(pk=self.alice.pk)

        # A bug in Django versions before 1.11 doesn't allow us to update a
        # field in terms of an annotation. Test that our queryset is able to
        # sort it out. See `AnnotableModelQuerySet.update`.
        alice_qs.update(
            details=Concat(
                F('full_name'),
                Value(" may have inspired Alice's Adventures in Wonderland.")
            )
        )
        self.assertIn(
            "Alice Liddell may have inspired ",
            alice_qs.values_list('details', flat=True)[0]
        )
