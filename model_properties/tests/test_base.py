from django.test import TestCase

from .app.models import Person

from ..base import ModelAnnotation


class ModelAnnotationTestCase(TestCase):
    def setUp(self):
        self.person = Person(first_name='Alice', last_name='Liddell')

    def test_expr(self):
        self.assertEquals(Person.full_name.expr,
                          Person.FULL_NAME_EXPR)

    def test_value(self):
        self.assertIsInstance(Person.full_name_as_annotation, ModelAnnotation)

        self.person.full_name_as_annotation = 'John Doe'
        self.assertEquals(self.person.full_name_as_annotation, 'John Doe')


class ModelPropertyTestCase(TestCase):
    def setUp(self):
        self.person = Person(first_name='Alice', last_name='Liddell')

    def test_func(self):
        self.assertTrue(callable(Person.full_name.func))

    def test_value(self):
        self.assertEquals(self.person.full_name, 'Alice Liddell')

        self.person.first_name = 'Bob'
        self.person.last_name = 'Dylan'
        self.assertEquals(self.person.full_name, 'Bob Dylan')

        self.person.full_name = 'Carl Sagan'
        self.assertEquals(self.person.full_name, 'Bob Dylan')
